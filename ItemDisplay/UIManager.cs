using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ItemDisplay
{
    public class UIManager : MonoBehaviour
    {
        private GameObject HUDRoot;
        private EnemySpawner enemySpawner;

        public GameObject ItemDisplayRoot;
        public Dictionary<CardData, SingleItem> ItemComponents = new Dictionary<CardData, SingleItem>();

        public void OnEnable()
        {
            On.Deck.AddCard += OnCardAdd;
            On.Deck.TakeCard += OnCardTake;

            enemySpawner = GetComponent<EnemySpawner>();

            HUDRoot = enemySpawner.timer.GetComponentInParent<Canvas>().gameObject;

            ItemDisplayRoot = new GameObject("ItemDisplayRoot", typeof(RectTransform), typeof(GridLayoutGroup));
            var rootTransform = ItemDisplayRoot.transform as RectTransform;
            var rootLayout = ItemDisplayRoot.GetComponent<GridLayoutGroup>();

            rootTransform.SetParent(HUDRoot.transform, false);

            rootTransform.anchorMin = new Vector2(1, 0);
            rootTransform.anchorMax = new Vector2(1, ModConfig.ItemDisplayHeight.Value);
            rootTransform.offsetMin = new Vector2(-ModConfig.ColumnWidth.Value*ModConfig.ColumnCount.Value-5, 5);
            rootTransform.offsetMax = new Vector2(-5, -5);

            rootLayout.startCorner = GridLayoutGroup.Corner.UpperLeft;
            rootLayout.startAxis = GridLayoutGroup.Axis.Horizontal;
            rootLayout.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
            rootLayout.constraintCount = ModConfig.ColumnCount.Value;

            rootLayout.cellSize = new Vector2(ModConfig.ColumnWidth.Value, ModConfig.RowHeight.Value);

            foreach(var entry in Deck.active.GetCards())
            {
                CreateEntry(entry.Key);
            }
        }

        public void OnDisable()
        {
            On.Deck.AddCard -= OnCardAdd;
            On.Deck.TakeCard -= OnCardTake;

            UnityEngine.Object.DestroyImmediate(ItemDisplayRoot);
        }

        private void OnCardAdd(On.Deck.orig_AddCard orig, Deck self, CardData cardData)
        {
            orig(self, cardData);

            UpdateEntry(cardData);
        }

        private void OnCardTake(On.Deck.orig_TakeCard orig, Deck self, CardData cardData)
        {
            orig(self, cardData);

            RemoveEntry(cardData);
        }

        public SingleItem CreateEntry(CardData cardData)
        {
            if(!Deck.active.HasCard(cardData)) return null;

            var entryObject = new GameObject($"ItemDisplay_{cardData.name}", typeof(RectTransform), typeof(SingleItem));
            var entryTransform = entryObject.transform as RectTransform;
            var singleItem = entryObject.GetComponent<SingleItem>();

            entryTransform.SetParent(ItemDisplayRoot.transform, false);

            singleItem.cardData = cardData;

            ItemComponents.Add(cardData, singleItem);
            return singleItem;
        }

        public void RemoveEntry(CardData cardData)
        {
            if(!ItemComponents.ContainsKey(cardData)) return;

            UnityEngine.Object.Destroy(ItemComponents[cardData].gameObject);
            ItemComponents.Remove(cardData);
        }

        public void UpdateEntry(CardData cardData)
        {
            if(!ItemComponents.ContainsKey(cardData))
            {
                CreateEntry(cardData);
                return;
            }

            ItemComponents[cardData].Refresh();
        }

        public class SingleItem : MonoBehaviour
        {
            public CardData cardData;
            private Image image;
            private TextMeshProUGUI text;

            public void Awake()
            {
                var imageObject = new GameObject($"{this.name}_Image", typeof(RectTransform), typeof(CanvasRenderer), typeof(Image));
                var imageTransform = imageObject.transform as RectTransform;
                image = imageObject.GetComponent<Image>();

                imageTransform.SetParent(this.transform, false);
                imageTransform.anchorMin = new Vector2(0, 0.5f);
                imageTransform.anchorMax = new Vector2(0, 0.5f);
                imageTransform.offsetMin = new Vector2(0, -ModConfig.ImageSize.Value/2);
                imageTransform.offsetMax = new Vector2(ModConfig.ImageSize.Value, ModConfig.ImageSize.Value/2);
                imageTransform.anchoredPosition = new Vector2(0, 0);

                var textObject = new GameObject($"{this.name}_Text", typeof(RectTransform), typeof(CanvasRenderer), typeof(TextMeshProUGUI));
                var textTransform = textObject.transform as RectTransform;
                text = textObject.GetComponent<TextMeshProUGUI>();

                textTransform.SetParent(this.transform, false);
                textTransform.anchorMin = new Vector2(0, 0);
                textTransform.anchorMax = new Vector2(1, 1);
                textTransform.offsetMin = new Vector2(2+ModConfig.ImageSize.Value, 0);
                textTransform.offsetMax = new Vector2(-2, 0);
                textTransform.anchoredPosition = new Vector2(0, 0);

                text.verticalAlignment = VerticalAlignmentOptions.Middle;
                text.horizontalAlignment = HorizontalAlignmentOptions.Left;
                text.fontSize = ModConfig.FontSize.Value;
                text.text = "x1";
            }

            public void Start()
            {
                image.sprite = cardData.sprite;
                image.color = cardData.color;
                Refresh();
            }

            public void Refresh()
            {
                text.text = $"x{Deck.active.GetCardAmount(cardData)}";
            }
        }
    }
}