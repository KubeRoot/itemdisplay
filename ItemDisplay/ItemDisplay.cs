﻿using BepInEx;

namespace ItemDisplay
{
    [BepInPlugin("com.kuberoot.itemdisplay", "ItemDisplay", "1.0.0")]
    public class ItemDisplay : BaseUnityPlugin
    {
        public void Awake()
        {
            ModConfig.InitConfig(Config);
        }

        public void OnEnable()
        {
            On.EnemySpawner.Start += OnRunStart;

            FindObjectOfType<EnemySpawner>()?.gameObject.AddComponent<UIManager>();

            ModConfig.ConfigurationChanged += OnConfigurationChanged;
        }

        public void OnDisable()
        {
            On.EnemySpawner.Start -= OnRunStart;
            ModConfig.ConfigurationChanged -= OnConfigurationChanged;

            var uiManager = FindObjectOfType<EnemySpawner>()?.gameObject.GetComponent<UIManager>();
            if(uiManager)
                UnityEngine.Object.Destroy(uiManager);
        }

        private void OnRunStart(On.EnemySpawner.orig_Start orig, EnemySpawner self)
        {
            orig(self);

            self.gameObject.AddComponent<UIManager>();
        }

        private void OnConfigurationChanged()
        {
            // Dirty way of allowing runtime config changes. Rebuilds the whole display on every change.
            var uiManager = FindObjectOfType<EnemySpawner>()?.gameObject.GetComponent<UIManager>();
            if(uiManager)
            {
                var uiManagerGameObject = uiManager.gameObject;
                UnityEngine.Object.DestroyImmediate(uiManager);
                uiManagerGameObject.AddComponent<UIManager>();
            }

        }
    }
}
