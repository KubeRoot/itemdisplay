using System;
using BepInEx.Configuration;

namespace ItemDisplay
{
    public static class ModConfig
    {
        internal static void InitConfig(ConfigFile config)
        {
        	ConfigEntry<T> Register<T>(ConfigEntry<T> entry) {
            	entry.SettingChanged += (_, __) => ConfigurationChanged?.Invoke();
            	return entry;
        	}

            ColumnCount = Register(config.Bind(new ConfigDefinition("General", "ColumnCount"), 2, new ConfigDescription("The amount of columns to use for displaying cards", new AcceptableValueRange<int>(1, 20))));
            ItemDisplayHeight = Register(config.Bind(new ConfigDefinition("General", "Height"), 0.75f, new ConfigDescription("The fraction of the screen height the card display will occupy", new AcceptableValueRange<float>(0, 1))));

            ColumnWidth = Register(config.Bind(new ConfigDefinition("Entry", "Width"), 40f, new ConfigDescription("The width of a single entry", new AcceptableValueRange<float>(0, 500f))));
            RowHeight = Register(config.Bind(new ConfigDefinition("Entry", "Height"), 20f, new ConfigDescription("The height of a single entry", new AcceptableValueRange<float>(0, 500f))));
            ImageSize = Register(config.Bind(new ConfigDefinition("Entry", "ImageSize"), 20f, new ConfigDescription("The size of the card image", new AcceptableValueRange<float>(0, 500f))));
            FontSize = Register(config.Bind(new ConfigDefinition("Entry", "FontSize"), 14f, new ConfigDescription("The size of the card count font", new AcceptableValueRange<float>(0, 500f))));
        }

        public static event Action ConfigurationChanged;

        public static ConfigEntry<int> ColumnCount;
        public static ConfigEntry<float> ItemDisplayHeight;

        public static ConfigEntry<float> ColumnWidth;
        public static ConfigEntry<float> RowHeight;
        public static ConfigEntry<float> ImageSize;
        public static ConfigEntry<float> FontSize;
    }
}