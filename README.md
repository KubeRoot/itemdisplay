# ItemDisplay

Adds a display showing cards in your deck on the side of the screen. Inspired by a similar feature in Binding of Isaac.

![Game screenshot](icon.png)

## Features:

- Various configuration options to tweak the size and positioning to your liking
- Includes hot-reload support, for both the whole mod and configuration

## Installation

Copy the `ItemDisplay` folder to `Neon Sundown/BepInEx/plugins`

## Patch notes: 

- 1.0.0 - Initial release